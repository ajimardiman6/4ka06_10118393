<!DOCTYPE html>
<html>

<head>
    <title>
        UTS Pemrograman Berbasis WEB
    </title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <table align="center">
        <tr>
            <td>
                <form onsubmit="event.preventDefault();onFormSubmit();" autocomplete="off">
                    <div>
                        <label>NAMA</label><label class="validation-error hide" id="fullNameValidationError">This field is required.</label>
                        <input type="text" name="fullName" id="fullName">
                    </div>
                    <div>
                        <label>NPM</label>
                        <input type="text" name="empCode" id="empCode">
                    </div>
                    <div>
                        <label>EMAIL</label>
                        <input type="text" name="salary" id="salary">
                    </div>
                    <div>
                        <label>ALAMAT</label>
                        <input type="text" name="city" id="city">
                    </div>
                    <div  class="form-action-buttons">
                        <input type="submit" value="Submit">
                    </div>
                </form>
            </td>
            <td>
                <table class="list" id="employeeList">
                    <thead>
                        <tr>
                            <th>NAMA</th>
                            <th>NPM</th>
                            <th>EMAIL</th>
                            <th>ALAMAT</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <script src="script.js"></script>
</body>

</html>